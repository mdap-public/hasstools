import pandas as pd

# Define the path to your Excel file and the CSV export path
excel_file_path = "~/The University of Melbourne/HASS Taskforce - Documents/Training/Web front door/NEW_tools_list.xlsx"
csv_export_path = "tools_list.csv"

# Load the Excel file into a Pandas DataFrame
df = pd.read_excel(excel_file_path)

# Filter the DataFrame by the "include" column (assuming you want to keep rows where "include" is True)
df = df[df['include'] == 1]
# Small complete csv for testing
# df = df[df['Branch2'].notna()]

# Delete the "include" column from the DataFrame
df = df.drop(columns=['include'])

# Export the DataFrame to a CSV file
df.to_csv(csv_export_path, index=False)

print("Filtered data has been exported to CSV.")
