function switchTab(index) {
    const tabs = document.getElementsByClassName('tab-pane');
    const buttons = document.getElementsByClassName('btn');

    for (let i = 0; i < tabs.length; i++) {
        tabs[i].classList.remove('show', 'active');
        buttons[i].classList.remove('active');
    }

    tabs[index].classList.add('show', 'active');
    buttons[index].classList.add('active');
}

// Tooltips
const tooltips = document.querySelectorAll('.tt')
tooltips.forEach(t => {
    new bootstrap.Tooltip(t)
})

// Tool modals
$(document).ready(function () {
    // Find all spans with class toolHook
    var toolHooks = document.getElementsByClassName('toolHook');

    // Function to show a modal based on the URL hash
    function showModalFromUrl() {
        var hash = window.location.hash;
        if (hash) {
            var modalId = 'modal_' + hash.substring(1);
            $('#' + modalId).modal('show');
        }
    }

    function removeHashFromUrl() {
        window.location.hash = '';
    }

    // Add click event listeners to each span to show modal boxes
    for (var i = 0; i < toolHooks.length; i++) {
        toolHooks[i].style.cursor = 'pointer';
        toolHooks[i].addEventListener('click', function () {
            var spanId = this.id;
            var uniqueString = spanId.substring(5);
            var modalId = 'modal_' + uniqueString;
            $('#' + modalId).modal('show');

            // Update the URL with the relevant hash when a modal is clicked
            window.location.hash = uniqueString;
        });
    }

    // Add click event listeners to each close button or cross to remove hash from url
    var close_elements = document.getElementsByClassName("close_modal");

    for (var i = 0; i < close_elements.length; i++) {
        close_elements[i].addEventListener('click', removeHashFromUrl, false);
    }

    // Show the corresponding modal when the page is loaded with a hash in the URL
    showModalFromUrl();
});

// Last updated
document.addEventListener('DOMContentLoaded', function() {
    var lastUpdatedElement = document.getElementById('lastUpdated');
    var lastUpdated = new Date(document.lastModified);
    var formattedDate = lastUpdated.toLocaleDateString('en-GB');
    lastUpdatedElement.textContent = 'Last updated: ' + formattedDate;
});